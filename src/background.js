if (window.location.host == "www.google.com") {
    var detect = setInterval(function () {
        var find = $('h3.r a')
        if (find.size()) {
            find.on("click", function (e) {
                e.preventDefault();
                var el = $(this)
                var data = el.data();
                if (data.href) {
                    window.open(data.href, '_blank');
                }
            });
            clearInterval(detect);
        }
    }, 50);
}